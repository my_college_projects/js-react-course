import React, { Component } from 'react';
import MyInputAutor from './components/MyInputAutor.js'
import $ from 'jquery';
import PubSub from 'pubsub-js';
import my_error from './My_Error';

export default class Autor extends Component {

  constructor() {
    super();
    this.state = { lista: [] };
  }

  componentDidMount() {
    /* esse component eh chamando antes do primeiro render */
    $.ajax({
      url: "http://localhost:8080/api/autores",
      dataType: 'json',
      success: function (response) {
        /* toda vez que mudamos o state, o metodo render
           eh chamado */
        this.setState({ lista: response });
      }.bind(this)
      /* o bind passa o this do React para o escopo do ajax*/
    });
    /* 
        o subscribe faz a inscrição no topico de atualização geral
        no callback recebemos o topico inscrito, e o objeto passado
    */
    PubSub.subscribe(
      'topico_atualizou_lista_de_autores',
      (topico, novaLista) => {
        this.setState({ lista: novaLista });
      }
    );
  }

  /* 
    o formularioAutor publica um novo autor cadastrado
    os dois components se comunicam
    essa tecnica é chamada de High Order Components
  */

  /*
    Quando criamos dois componentes separados, também movemos o estado da lista para o componente de listagem de autores. 
    Quando fizemos isso, o componente responsável pelo formulário do autor parou de ter acesso ao estado da listagem e, por consequência, 
    também parou de poder invocar a função setState para atualizar esses dados.

    Para resolver isso criamos um terceiro componente, que chamamos de AutorBox que mantém o estado da listagem e só repassa para os seus componentes filhos. 
    Essa é uma solução conhecida como High Order Components e muito praticada em projetos que utilizam o React. A ideia é que o estado fique sempre nos seus High Order Components e que os outros componentes fiquem apenas com a responsabilidade de montar a visualização em si.

    Completando, no momento que você retira a parte do AJAX do seu componente você acaba de dar a ele mais chances de ser reaproveitado em outras situações. 
    Já que agora ele é baseado apenas em um JSON passado para ele, de onde vem acaba não sendo importante.

  */
  render() {
    return (
      <div>
        <div className="header">
          <h1>Cadastro de Autores</h1>
        </div>
        <div className="content" id="content">
          <FormularioAutor />
          <TabelaAutores lista={this.state.lista} />
        </div>
      </div>
    )
  }
}

class TabelaAutores extends Component {
  render() {
    return (
      <div>
        <table className="pure-table">
          <thead>
            <tr>
              <th>Nome</th>
              <th>email</th>
            </tr>
          </thead>
          <tbody>
            {
              /* map : mapeia o vetor lista para
                outro vetor, no caso uma lista html tr*/
              this.props.lista.map(function (autor) {
                return (
                  <tr key={autor.id}>
                    <td>{autor.name}</td>
                    <td>{autor.email}</td>
                  </tr>
                );
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

class FormularioAutor extends Component {

  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      password: ''
    };

    /* predefinimos aqui o escopo com o this das functions , para evitar*/
    /* atribuicao dinamica - o que poderia gerar lentidao */
    this.setName = this.setName.bind(this)
    this.setEmail = this.setEmail.bind(this);
    this.setPassword = this.setPassword.bind(this);
    this.sendForm = this.sendForm.bind(this);
  }

  sendForm(event) {
    event.preventDefault();
    $.ajax({
      url: 'http://localhost:8080/api/autores',
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      type: 'POST',
      data: JSON.stringify({
        name: this.state.name,
        email: this.state.email,
        password: this.state.password
      }),
      success: function (novaListagem) {
        /* queremos disparar um aviso geral , para todos components que "quiserem" ouvir, que a lista atualizou */
        /*
            o publish vai registrar um topico de atualizações chamado "topico_atualizou_lista_de_autores" que uma nova listagem está disponível
            pode haver objetos que estejam registrados nesse topico
            os objetos inscritos podem vê essa atualização
        */
        PubSub.publish('topico_atualizou_lista_de_autores', novaListagem);
      },
      error: function (response) {
        if (response.status === 400) {
          new my_error().publishError('autor',response.responseJSON);
        }
      },
      beforeSend: function () {
        PubSub.publish("topico_de_limpar_mensagens_de_erros_no_formulario_autores", {});
      }
    });
  }

  setName(event) {
    this.setState({ name: event.target.value });
  }

  setEmail(event) {
    this.setState({ email: event.target.value });
  }

  setPassword(event) {
    this.setState({ password: event.target.value });
  }


  /* 
    no render, os argumentos name id podem ser acessado no component MyInputAutor atravez
    da propriedade this.props.name e this.props.id respectivamente
  */
  render() {
    return (
      <div className="pure-form pure-form-aligned">
        <form className="pure-form pure-form-aligned" onSubmit={this.sendForm} method="post">
          <MyInputAutor id="nome" type="text" name="nome" onChange={this.setName} label="Nome" />
          <MyInputAutor id="email" type="email" name="email" onChange={this.setEmail} label="Email" />
          <MyInputAutor id="senha" type="password" name="senha" onChange={this.setPassword} label="Senha" />
          <div className="pure-control-group">
            <label></label>
            <button type="submit" className="pure-button pure-button-primary">Gravar</button>
          </div>
        </form>
      </div>
    );
  }
}