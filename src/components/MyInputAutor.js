import React, { Component } from 'react';
import PubSub from 'pubsub-js';

export default class MyInputAutor extends Component{

    constructor(){
        super();
        this.state = {message_erro: ''}
    }

    componentDidMount(){
        PubSub.subscribe(
            "erro_validacao_autores",
            (topico,erro)=>{
                console.log(erro);
                console.log(this.props.name);
                /* filtra a mensagem de erro */
                if( (this.props.name === "nome") && (erro.name === '&') ){
                    this.setState({message_erro:'Campo deve ser preenchido'})
                }else if( (this.props.name === "email") && (erro.email === '&') ){
                    this.setState({message_erro:'Campo deve ser preenchido'})
                }else if ( (this.props.name === "senha") && (erro.password === '&') ){
                    this.setState({message_erro:'Campo deve ser preenchido'})
                }
            }
        );

        /* inscrevemos para limpar o spam*/
        PubSub.subscribe(
            "topico_de_limpar_mensagens_de_erros_no_formulario_autores",
            (topico,erro)=>{
                this.setState({message_erro:''})
            }
        );
    }
    render(){
        return(
            <div className="pure-control-group">
                  <label htmlFor={this.props.id}>{this.props.label}</label>
                  {/* <input id={this.props.id} type={this.props.type} name={this.props.name} onChange={this.props.onChange} />  podemos substituir para spread operator*/}
                  <input {...this.props}/>
                  <span className="erro_validacao">{this.state.message_erro}</span>

            </div>
        );
    }
}