import React, { Component } from 'react';
import PubSub from 'pubsub-js';

export default class MyInputLivros extends Component{

    constructor(){
        super();
        this.state = {message_erro: ''}
    }

    componentDidMount(){
        PubSub.subscribe(
            "erro_validacao_livros",
            (topico,erro)=>{
                /* filtra a mensagem de erro */
                if( (this.props.name === "title") && (erro.title === '&') ){
                    this.setState({message_erro:'Campo deve ser preenchido'})
                }else if ( (this.props.name === "price") && (erro.price === '&') ){
                    this.setState({message_erro:'Campo deve ser preenchido'})
                }
            }
        );

        /* increvemos para limpar o spam*/
        PubSub.subscribe(
            "topico_de_limpar_mensagens_de_erros_no_formulario_livros",
            (topico,erro)=>{
                this.setState({message_erro:''})
            }
        );
    }
    render(){
        return(
            <div className="pure-control-group">
                <label htmlFor={this.props.id}>{this.props.label}</label>
                <input id={this.props.id} type={this.props.type} name={this.props.name} onChange={this.props.onChange} />
                <span className="erro_validacao">{this.state.message_erro}</span>
            </div>
        );
    }
        
}