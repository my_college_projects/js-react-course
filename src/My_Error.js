import PubSub from 'pubsub-js';

export default class My_Error{
    publishError(selector,response_erros){
        if(selector === 'autor'){
            PubSub.publish("erro_validacao_autores",response_erros);
        }else if(selector === 'livro'){
            PubSub.publish("erro_validacao_livros",response_erros);
        }
        
    }
}