import React, { Component } from 'react';
import './css/side-menu.css';
import './css/pure-min.css';

export default class Home extends Component {
    render() {
        return (
            <div>
                <div className="header">
                    <h1>Seja bem vindo</h1>
                </div>
                <div className="content" id="content"></div>
            </div>
        );
    }
}