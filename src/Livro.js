import React, { Component } from 'react';
import $ from 'jquery';
import PubSub from 'pubsub-js';
import my_error from './My_Error';
import MyInputLivro from './components/MyInputLivros';
import './css/side-menu.css';
import './css/pure-min.css';

export default class Livro extends Component {

  constructor() {
    super();
    this.state = {
      lista_livros: [],
      lista_autores: []
    };
  }

  componentDidMount() {
    
    $.ajax({
      url: "http://localhost:8080/api/livros",
      dataType: 'json',
      success: function (response) {
        this.setState({
          lista_livros: response
        })
      }.bind(this)
    });

    $.ajax({
      url: "http://localhost:8080/api/autores",
      dataType: 'json',
      success: function (response) {
        this.setState({
          lista_autores: response
        })
      }.bind(this)
    });

    PubSub.subscribe(
      'topico_atualizou_lista_de_livros',
      (topico, novaLista) => {
        this.setState({
          lista_livros: novaLista
        })
      }
    );
    
  }


  render() {
    return (
      <div>
        <div className="header">
          <h1>Cadastro de Livros</h1>
        </div>
        <div className="content" id="content">
          <FormularioLivros lista_autores={this.state.lista_autores} />
          <TabelaLivros lista_livros={this.state.lista_livros} lista_autores={this.state.lista_autores} />
        </div>
      </div>
    );
  }
}

class TabelaLivros extends Component {

  constructor() {
    super();
    this.state = {
      tabela_title: [],
      tabela_price: [],
      tabela_autor_name: []
    };
  }

  
  
  /* retornar o nome do autor pelo id do livro */
  get_by_id(id){
    let array = this.props.lista_autores;
    let name = 'not found';
    for (let i = 0 ; i < array.length ; i++) {
        if(array[i].id == id){
        
            name = array[i].name;
            break;
        }
    }
    
    return name;
 }

  render() {
    return (
      <div>
        <table className="pure-table">
          <thead>
            <tr>
              <th>Título</th>
              <th>Preço</th>
              <th>Autor</th>
            </tr>
          </thead>
          <tbody>
            {
              /* map : mapeia o vetor lista para
                outro vetor, no caso uma lista html tr*/
              this.props.lista_livros.map(function (livro) {
                return (
                  <tr key={livro.id}>
                    <td>{livro.title}</td>
                    <td>R$ {livro.price}</td>
                    <td>{this.get_by_id(livro.id_autor)}</td>
                  </tr>
                );
              },this)
            }
          </tbody>
        </table>
      </div>
    );
  }
}

class FormularioLivros extends Component {

  constructor() {
    super();
    this.state = {
      title: '',
      id_autor: '1',
      price: ''

    };

    this.setTitle = this.setTitle.bind(this)
    this.setPrice = this.setPrice.bind(this);
    this.setIdAutor = this.setIdAutor.bind(this);
    this.sendForm = this.sendForm.bind(this);
  }

  sendForm(event) {
    event.preventDefault();
    $.ajax({
      url: 'http://localhost:8080/api/livros',
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      type: 'POST',
      data: JSON.stringify({
        title: this.state.title,
        price: this.state.price,
        id_autor: this.state.id_autor
      }),
      success: function (novaListagem) {
        PubSub.publish('topico_atualizou_lista_de_livros', novaListagem);
        //PubSub.publish('topico_de_limpar_input', {});
      },
      error: function (response) {
        if (response.status === 400) {
          new my_error().publishError('livro',response.responseJSON);
        }
      },
      beforeSend: function () {
        PubSub.publish("topico_de_limpar_mensagens_de_erros_no_formulario_livros", {});
      }
    });
  }

  setTitle(event) {
    this.setState({ title: event.target.value });
  }

  setIdAutor(event) {
    console.log(event.target.value);
    this.setState({ id_autor: event.target.value });
  }

  setPrice(event) {
    this.setState({ price: event.target.value });
  }


  /* 
    no render, os argumentos name id podem ser acessado no component MyInputLivro atravez
    da propriedade this.props.name e this.props.id respectivamente
  */
  render() {
    return (
      <div className="autorForm">
        <form className="pure-form pure-form-aligned" onSubmit={this.sendForm} method="post">
          <MyInputLivro id="title" type="text" name="title" onChange={this.setTitle} label="Título" />
          <MyInputLivro id="price" type="text" name="price" onChange={this.setPrice} label="Preço" />
          <div className="pure-control-group">
              <label htmlFor={"autor"}>{"Autor"}</label>
              <select onChange={this.setIdAutor} name={"autor"}>
                {
                  this.props.lista_autores.map(function (autor) {
                      return (
                          <option key={autor.id} value={autor.id}>{autor.name}</option>
                      );
                  })
                }
              </select>
          </div>
          <div className="pure-control-group">
            <label></label>
            <button type="submit" className="pure-button pure-button-primary">Gravar</button>
          </div>
        </form>
      </div>
    );
  }
}

