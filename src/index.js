import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Home from './Home';
import Livro from './Livro';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Autor from './Autor.js'

ReactDOM.render(
    (
        <Router>
            <App>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/autor" component={Autor} />
                    <Route path="/livro" component={Livro} />
                </Switch>
            </App>
        </Router>
    ),
    document.getElementById('root')
);


/*

O Router recebe apenas um componente, que pode ser qualquer coisa. 
O fato primordial é que dentro desse componente exista alguma 
declaração de rotas, para que os endereços continuem funcionando. 
Perceba que foi justamente isso que fizemos quando passamos o 
componente App como filho de Router o Switch 
(que já vai ser explicado) como argumento da App.

Um segundo ponto importante é a utilização do componente Switch. 
Poderíamos simplesmente deixar várias tag Route como parâmetro 
da nossa App, dessa forma as rotas seriam registradas e tudo 
funcionaria. O problema é que nesse caso queremos deixar claro 
que apenas uma das rotas vai ser acionada, o Switch serve 
justamente para garantir isso.

Da forma que fizemos, mantemos exatamente o comportamento da 
versão anterior da nossa aplicação, só que usando a versão 
mais nova do router!

*/